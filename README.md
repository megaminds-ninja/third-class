# Android Ninja - Class No: 3 #
Date: 25 August, 2017

## Topics covered this class:

- ImageView with Android Piccasso Library
- SharedPreference
- WebView
- Send an E-mail using Intent
- Discussing about Singleton Design Pattern
- Discussing about Android compile and build system. Keyword: Dalvik